//
//  MapViewController.swift
//  AITUapp
//
//  Created by Nurba on 22.04.2021.
//

import UIKit
import YandexMapsMobile

class MapViewController: UIViewController{
 
    @IBOutlet weak var Mapview: YMKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Mapview.mapWindow.map.isRotateGesturesEnabled = false
        Mapview.mapWindow.map.move(
               with: YMKCameraPosition.init(target: YMKPoint(latitude: 51.169392, longitude: 71.449074), zoom: 15, azimuth: 0, tilt: 0),
               animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 5),
               cameraCallback: nil)
        let scale = UIScreen.main.scale
        let mapKit = YMKMapKit.sharedInstance()
        let userLocationLayer = mapKit.createUserLocationLayer(with: Mapview.mapWindow)


        userLocationLayer.setVisibleWithOn(true)
        userLocationLayer.isHeadingEnabled = true
       
        userLocationLayer.setObjectListenerWith(self)


        
        let point2 = YMKPoint(latitude: 51.0908492, longitude: 71.4183229)
        let mapObject = Mapview.mapWindow.map.mapObjects
        mapObject.clear()
        let placemark2 = mapObject.addPlacemark(with: point2)
                   placemark2.setIconWith(UIImage(named: "aitu")!)
    }

}

extension MapViewController: YMKUserLocationObjectListener{
    
    func onObjectAdded(with view: YMKUserLocationView) {
        view.arrow.setIconWith(UIImage(named:"UserArrow")!)
        
        let pinPlacemark = view.pin.useCompositeIcon()
        
        pinPlacemark.setIconWithName("UserArrow",
            image: UIImage(named:"UserArrow")!,
            style:YMKIconStyle(
                anchor: CGPoint(x: 0, y: 0) as NSValue,
                rotationType:YMKRotationType.rotate.rawValue as NSNumber,
                zIndex: 0,
                flat: true,
                visible: true,
                scale: 1.5,
                tappableArea: nil))
    }
    
    func onObjectRemoved(with view: YMKUserLocationView) {}
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {}
}
