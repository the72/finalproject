//
//  WeatherViewController.swift
//  AITUapp
//
//  Created by Nurba on 22.04.2021.
//

import UIKit
import Alamofire
import FirebaseAuth

class WeatherViewController: UIViewController {

    var decoder: JSONDecoder = JSONDecoder()
    
    var myData: Welcome!
   
    
  
    
    let url = "http://10.10.10.153/course/view.php?id=2"
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.cornerRadius = 13
        tableView.layer.masksToBounds = true
        tableView.register(WidgetCell.nib, forCellReuseIdentifier: WidgetCell.identifier)
        getData()
     
       
       }


    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        handleNotAuth()
        
       
    }


    private func handleNotAuth() {
        if Auth.auth().currentUser == nil{
            let loginVC = LoginViewController()
            loginVC.modalPresentationStyle = .fullScreen
            present(loginVC, animated: false)
        }
    }

    

    
    func getData(){
        AF.request(url).responseJSON { (response) in
            switch response.result{
            case .success(_):
                guard let data = response.data else { return }
                guard let answer = try? self.decoder.decode(Welcome.self, from: data) else{ return }
                print(answer)
                self.myData = answer
                self.tableView.reloadData()
            case .failure(let err):
                print(err.errorDescription ?? "")
            }
        }
    }

}

extension WeatherViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: WidgetCell.identifier, for: indexPath)
as! WidgetCell
        
  
        
       
        return cell
    }

    
    
}
